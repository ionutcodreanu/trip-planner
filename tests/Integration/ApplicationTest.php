<?php

namespace TripSorting\Tests\Integration;

use PHPUnit\Framework\TestCase;
use TripSorting\Application\Application;
use TripSorting\Application\Assets\Bus;
use TripSorting\Application\Assets\Flight;
use TripSorting\Application\Assets\Train;
use TripSorting\Application\BoardFactory;
use TripSorting\BoardSorter;

class ApplicationTest extends TestCase
{
    /** @var Application */
    private $application;

    protected function setUp()
    {
        $this->application = new Application(
            new BoardSorter(),
            new BoardFactory()
        );
    }

    /**
     * given a list of one board print them sorted
     */
    public function testGivenAListOfOneBoardPrintThemSorted()
    {
        $flight = new Flight();
        $flight->origin = 'Munich';
        $flight->destination = 'Madrid';
        $flight->number = 'SK22';
        $flight->seat = '25B';
        $flight->gate = '25';
        $flight->baggagePolicy = Application::TRANSPORT_FLIGHT_BAGGAGE_POLICY_TRANSFERRED;
        $inputData = [$flight];

        $expectedMessage = "1.\tFrom Munich, take flight SK22 to Madrid. Gate 25, seat 25B." . PHP_EOL .
            "Baggage will be automatically transferred from your last leg." . PHP_EOL
            . "2.\tYou have arrived at your final destination.";
        $this->assertEquals($expectedMessage, $this->application->showItinerary($inputData));
    }

    /**
     * given a list of three sorted boards print them sorted
     */
    public function testGivenAListOfThreeSortedBoardsPrintThemSorted()
    {
        $flight = new Flight();
        $flight->origin = 'Munich';
        $flight->destination = 'Madrid';
        $flight->number = 'SK22';
        $flight->seat = '25B';
        $flight->gate = '25';
        $flight->baggagePolicy = Application::TRANSPORT_FLIGHT_BAGGAGE_POLICY_TRANSFERRED;

        $bus = new Bus();
        $bus->origin = 'Madrid';
        $bus->destination = 'Barcelona';
        $bus->seat = '25B';

        $train = new Train();
        $train->origin = 'Barcelona';
        $train->destination = 'Paris';
        $train->number = '25G';
        $train->seat = '25B';
        $inputData = [$flight, $bus, $train];


        $expectedMessage =
            "1.\tFrom Munich, take flight SK22 to Madrid. Gate 25, seat 25B." . PHP_EOL .
            'Baggage will be automatically transferred from your last leg.' . PHP_EOL
            . "2.\tTake the airport bus from Madrid to Barcelona. Sit in seat 25B." . PHP_EOL
            . "3.\tTake train 25G from Barcelona to Paris. Sit in seat 25B." . PHP_EOL
            . "4.\tYou have arrived at your final destination.";
        $this->assertEquals($expectedMessage, $this->application->showItinerary($inputData));
    }

    /**
     * given a list of three inverse sorted boards print them sorted
     */
    public function testGivenAListOfThreeInverseSortedBoardsPrintThemSorted()
    {
        $flight = new Flight();
        $flight->origin = 'Munich';
        $flight->destination = 'Madrid';
        $flight->number = 'SK22';
        $flight->seat = '25B';
        $flight->gate = '25';
        $flight->baggagePolicy = Application::TRANSPORT_FLIGHT_BAGGAGE_POLICY_DROP;
        $flight->ticketNumber = '224';

        $bus = new Bus();
        $bus->origin = 'Madrid';
        $bus->destination = 'Barcelona';
        $bus->seat = '25B';

        $train = new Train();
        $train->origin = 'Barcelona';
        $train->destination = 'Paris';
        $train->number = '25G';
        $inputData = [$flight, $bus, $train];

        $expectedMessage =
            "1.\tFrom Munich, take flight SK22 to Madrid. Gate 25, seat 25B." . PHP_EOL .
            'Baggage drop at ticket counter 224.' . PHP_EOL
            . "2.\tTake the airport bus from Madrid to Barcelona. Sit in seat 25B." . PHP_EOL
            . "3.\tTake train 25G from Barcelona to Paris. No seat assignment." . PHP_EOL
            . "4.\tYou have arrived at your final destination.";
        $this->assertEquals($expectedMessage, $this->application->showItinerary(array_reverse($inputData)));
    }
}
