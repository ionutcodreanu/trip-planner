<?php

namespace TripSorting\Tests\Unit;

use PHPUnit\Framework\TestCase;
use TripSorting\Cards\Flight;

class FlightTest extends TestCase
{

    /**
     * given flight number SK22 from Stockholm to New York JFK at gate 22 and seat 7B with automatic baggage transfer format to specific message
     */
    public function testGivenFlightNumberSK22FromStockholmToNewYorkJFKAtGate22AndSeat7BWithAutomaticBaggageTransferFormatToSpecificMessage(
    )
    {
        $flight = Flight::createFrom(
            'Stockholm',
            'New York JFK',
            'SK22',
            '22',
            '7B',
            Flight::BAGGAGE_TRANSFERRED
        );

        static::assertEquals(
            'From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B.' . PHP_EOL
            . 'Baggage will be automatically transferred from your last leg.',
            (string)$flight
        );
    }

    /**
     * given flight number WIZZ123 from Vienna to Hong Kong at gate 23 and seat 7C with baggage drop at 344 format to specific message
     */
    public function testGivenFlightNumberWIZZ123FromViennaToHongKongAtGate23AndSeat7CWithBaggageDropAt344FormatToSpecificMessage()
    {
        $flight = Flight::createFrom(
            'Vienna',
            'Hong Kong',
            'WIZZ123',
            '23',
            '7C',
            Flight::BAGGAGE_DROP,
            '344'
        );

        static::assertEquals(
            'From Vienna, take flight WIZZ123 to Hong Kong. Gate 23, seat 7C.' . PHP_EOL
            . 'Baggage drop at ticket counter 344.',
            (string)$flight
        );
    }

}
