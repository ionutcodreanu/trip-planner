<?php

namespace TripSorting\Tests\Unit;

use PHPUnit\Framework\TestCase;
use TripSorting\BoardSorter;
use TripSorting\SortingFailure;

class SortingTest extends TestCase
{
    /** @var BoardSorter */
    private $sorting;

    protected function setUp()
    {
        $this->sorting = new BoardSorter();
    }

    /**
     * given one boarding card return it
     */
    public function testGivenOneBoardingCardReturnIt()
    {
        $boardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Bucharest',
            'Barcelona',
            '75A'
        );

        $sortedBoardCards = $this->sorting->sort([$boardCard]);
        static::assertEquals([$boardCard], $sortedBoardCards);
    }

    /**
     * given a empty list of board cards should return an empty list
     */
    public function testGivenAEmptyListOfBoardCardsShouldReturnAnEmptyList()
    {
        $sortedBoardCards = $this->sorting->sort([]);
        static::assertEquals([], $sortedBoardCards);
    }

    /**
     * given a list with two sorted board cards should return the list
     */
    public function testGivenAListWithTwoSortedBoardCardsShouldReturnTheList()
    {
        $firstBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Bucharest',
            'Barcelona',
            '75A'
        );

        $secondBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Barcelona',
            'Paris',
            '75A'
        );

        $sortedBoardCards = $this->sorting->sort([$firstBoardCard, $secondBoardCard]);
        static::assertEquals([$firstBoardCard, $secondBoardCard], $sortedBoardCards);
    }

    /**
     * given a list with three sorted board cards should return the list
     */
    public function testGivenAListWithThreeSortedBoardCardsShouldReturnTheList()
    {
        $firstBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Bucharest',
            'Barcelona',
            '75A'
        );

        $secondBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Barcelona',
            'Paris',
            '75A'
        );
        $thirdBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Paris',
            'Rome',
            '75A'
        );

        $boardCards = [$firstBoardCard, $secondBoardCard, $thirdBoardCard];
        $sortedBoardCards = $this->sorting->sort($boardCards);
        static::assertEquals($boardCards, $sortedBoardCards);
    }

    /**
     * given a list with three board cards in inverse order should return the list sorted
     */
    public function testGivenAListWithThreeBoardCardsInInverseOrderShouldReturnTheListSorted()
    {
        $firstBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Bucharest',
            'Barcelona',
            '75A'
        );

        $secondBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Barcelona',
            'Paris',
            '75A'
        );
        $thirdBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Paris',
            'Rome',
            '75A'
        );

        $boardCards = [$firstBoardCard, $secondBoardCard, $thirdBoardCard];
        $sortedBoardCards = $this->sorting->sort(array_reverse($boardCards));
        static::assertEquals($boardCards, $sortedBoardCards);
    }

    /**
     * given a list with three board cards in random order should return the list sorted
     */
    public function testGivenAListWithThreeBoardCardsInRandomOrderShouldReturnTheListSorted()
    {
        $firstBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Bucharest',
            'Barcelona',
            '75A'
        );

        $secondBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Barcelona',
            'Paris',
            '75A'
        );
        $thirdBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Paris',
            'Rome',
            '75A'
        );

        $boardCards = [$firstBoardCard, $secondBoardCard, $thirdBoardCard];
        $clonedList = $boardCards;
        shuffle($clonedList);
        $sortedBoardCards = $this->sorting->sort($clonedList);
        static::assertEquals($boardCards, $sortedBoardCards);
    }

    /**
     * given a invalid trip list throw an exception
     *
     */
    public function testGivenAInvalidTripListThrowAnException()
    {
        $firstBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Bucharest',
            'Barcelona',
            '75A'
        );

        $secondBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Barcelona',
            'Paris',
            '75A'
        );
        $thirdBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Paris',
            'Rome',
            '75A'
        );
        $fourthBoardCard = \TripSorting\Cards\AirportBus::createFrom(
            'Rome',
            'Bucharest',
            '75A'
        );

        $boardCards = [$firstBoardCard, $secondBoardCard, $thirdBoardCard, $fourthBoardCard];
        $clonedList = $boardCards;
        shuffle($clonedList);

        $this->expectException(SortingFailure::class);
        $this->expectExceptionMessage('Unable to calculate the trip');
        $this->sorting->sort($clonedList);
    }
}
