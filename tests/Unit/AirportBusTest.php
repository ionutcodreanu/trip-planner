<?php

namespace TripSorting\Tests\Unit;

use PHPUnit\Framework\TestCase;

class AirportBusTest extends TestCase
{

    /**
     * given an airport bus from Barcelona to Gerona Airport with seat 45A format to specific message
     */
    public function testGivenAnAirportBusFromBarcelonaToGeronaAirportWithSeat45AFormatToSpecificMessage()
    {
        $bus = \TripSorting\Cards\AirportBus::createFrom(
            'Barcelona',
            'Gerona Airport',
            '45A'
        );

        static::assertEquals(
            'Take the airport bus from Barcelona to Gerona Airport. Sit in seat 45A.',
            (string)$bus
        );
    }


    /**
     * given an airport bus from Valencia to Lille with seat 45B format to specific message
     */
    public function testGivenAnAirportBusFromValenciaToLilleWithSeat45BFormatToSpecificMessage()
    {
        $bus = \TripSorting\Cards\AirportBus::createFrom(
            'Valencia',
            'Lille',
            '45B'
        );

        static::assertEquals(
            'Take the airport bus from Valencia to Lille. Sit in seat 45B.',
            (string)$bus
        );
    }

    /**
     * given an airport bus from Capri to Naples with no seat format to specific message
     */
    public function testGivenAnAirportBusFromCapriToNaplesWithNoSeatFormatToSpecificMessage()
    {
        $bus = \TripSorting\Cards\AirportBus::createFrom(
            'Capri',
            'Naples'
        );

        static::assertEquals(
            'Take the airport bus from Capri to Naples. No seat assignment.',
            (string)$bus
        );
    }
}
