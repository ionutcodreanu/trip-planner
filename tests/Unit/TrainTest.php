<?php

namespace TripSorting\Tests\Unit;

use PHPUnit\Framework\TestCase;
use TripSorting\Cards\Train;

class TrainTest extends TestCase
{
    /**
     * given the train 78A from Madrid to Barcelona with the seat 45B format to specific message
     */
    public function testGivenTheTrain78AFromMadridToBarcelonaWithTheSeat45BFormatToSpecificMessage()
    {
        $train = Train::createFrom(
            'Madrid',
            'Barcelona',
            '78A',
            '45B'
        );
        static::assertEquals(
            'Take train 78A from Madrid to Barcelona. Sit in seat 45B.',
            (string)$train
        );
    }

    /**
     * given the train 78B from Paris to Amsterdam with the seat 45C format to specific message
     */
    public function testGivenTheTrain78BFromParisToAmsterdamWithTheSeat45CFormatToSpecificMessage()
    {
        $train = Train::createFrom(
            'Paris',
            'Amsterdam',
            '78B',
            '45C'
        );
        static::assertEquals(
            'Take train 78B from Paris to Amsterdam. Sit in seat 45C.',
            (string)$train
        );
    }


    /**
     * given the train 78C from Munich to Salzburg with no seat format to specific message
     */
    public function testGivenTheTrain78CFromMunichToSalzburgWithNoSeatFormatToSpecificMessage()
    {
        $train = Train::createFrom(
            'Munich',
            'Salzburg',
            '78C',
            ''
        );
        static::assertEquals(
            'Take train 78C from Munich to Salzburg. No seat assignment.',
            (string)$train
        );
    }
}
