# Trip sorting problem

You are given a stack of boarding cards for various transportations that will take you from a point
A to point B via several stops on the way. All of the boarding cards are out of order and you don't
know where your journey starts, nor where it ends. Each boarding card contains information
about seat assignment, and means of transportation (such as flight number, bus number etc).
Write an API that lets you sort this kind of list and present back a description of how to complete
your journey.
For instance the API should be able to take an unordered set of boarding cards, provided in a
format defined by you, and produce this list:
1. Take train 78A from Madrid to Barcelona. Sit in seat 45B.
2. Take the airport bus from Barcelona to Gerona Airport. No seat assignment.
3. From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A.
Baggage drop at ticket counter 344.
4. From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B.
Baggage will we automatically transferred from your last leg.
5. You have arrived at your final destination.
The list should be defined in a format that's compatible with the input format.
The API is to be an internal PHP API so it will only communicate with other parts of a PHP
application, not server to server, nor server to client. Use PHP-doc to document the input and
output your API accepts / returns.

## Usage

In order to be able to test project you need [Composer](https://getcomposer.org/) and PHP version >=7.2(tested only with 7.2).
After cloning the project run following command:

```bash
composer install
```

After that run the tests using following command:
```bash
./vendor/bin/phpunit --testsuite=all
```

## External application usage

The package ca be used using the **\TripSorting\Application\App** facade and providing the transportable assets from the
**\TripSorting\Application\Assets** namespace. Bellow is a sample usage code:
```php
<?php

    $flight = new \TripSorting\Application\Assets\Flight();
    $flight->origin = 'Munich';
    $flight->destination = 'Madrid';
    $flight->number = 'SK22';
    $flight->seat = '25B';
    $flight->gate = '25';
    $flight->baggagePolicy = \TripSorting\Application\Application::TRANSPORT_FLIGHT_BAGGAGE_POLICY_DROP;
    $flight->ticketNumber = '224';

    $bus = new \TripSorting\Application\Assets\Bus();
    $bus->origin = 'Madrid';
    $bus->destination = 'Barcelona';
    $bus->seat = '25B';

    $train = new \TripSorting\Application\Assets\Train();
    $train->origin = 'Barcelona';
    $train->destination = 'Paris';
    $train->number = '25G';
    $inputData = [$flight, $bus, $train];
    
    echo \TripSorting\Application\App::showItinerary([$train, $bus, $flight]);
```

## Implementation assumptions and limitations

The sorting algorithm make the assumptions that always is a continuous trip in the list and a city is not visited twice.
