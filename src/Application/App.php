<?php

namespace TripSorting\Application;

use TripSorting\BoardSorter;

/**
 * Application facade for easy usage outside the project
 *
 * @package TripSorting\Application
 */
class App
{
    /** @var Application */
    private static $app = null;

    private static function getApp(): Application
    {
        if (is_null(static::$app)) {
            static::$app = new Application(
                new BoardSorter(),
                new BoardFactory()
            );
        }

        return static::$app;
    }

    /**
     * @param Transportable[] $boards
     * @return string
     * @throws InvalidBoardType
     * @throws \TripSorting\SortingFailure
     */
    public static function showItinerary(array $boards)
    {
        return self::getApp()->showItinerary($boards);
    }
}
