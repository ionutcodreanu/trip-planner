<?php

namespace TripSorting\Application\Assets;

use TripSorting\Application\Application;
use TripSorting\Application\Transportable;

class Flight implements Transportable
{
    /** @var string */
    public $origin;
    /** @var string */
    public $destination;
    /** @var string */
    public $number;
    /** @var string */
    public $seat;
    /** @var string */
    public $gate;
    /** @var string */
    public $baggagePolicy = Application::TRANSPORT_FLIGHT_BAGGAGE_POLICY_TRANSFERRED;
    /** @var string */
    public $ticketNumber = '';
}
