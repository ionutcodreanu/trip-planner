<?php

namespace TripSorting\Application\Assets;

use TripSorting\Application\Transportable;

class Bus implements Transportable
{
    /** @var string */
    public $origin;
    /** @var string */
    public $destination;
    /** @var string */
    public $seat;
}
