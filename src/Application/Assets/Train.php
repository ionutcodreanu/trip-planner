<?php

namespace TripSorting\Application\Assets;

use TripSorting\Application\Transportable;

class Train implements Transportable
{
    /** @var string */
    public $origin;
    /** @var string */
    public $destination;
    /** @var string */
    public $number;
    /** @var string */
    public $seat = '';
}
