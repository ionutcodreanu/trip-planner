<?php

namespace TripSorting\Application;

use TripSorting\Cards\AirportBus;
use TripSorting\Cards\BoardCard;
use TripSorting\Cards\Flight;
use TripSorting\Cards\Train;
use TripSorting\Application\Assets\Flight as FlightAsset;
use TripSorting\Application\Assets\Train as TrainAsset;
use TripSorting\Application\Assets\Bus as BusAsset;

class BoardFactory
{
    /**
     * @param Transportable $transportable
     * @return BoardCard
     * @throws InvalidBoardType
     */
    public function createFrom(Transportable $transportable): BoardCard
    {
        switch (get_class($transportable)) {
            case FlightAsset::class:
                /** @var FlightAsset $transportable */
                return Flight::createFrom(
                    $transportable->origin,
                    $transportable->destination,
                    $transportable->number,
                    $transportable->gate,
                    $transportable->seat,
                    $transportable->baggagePolicy,
                    $transportable->ticketNumber
                );
                break;
            case BusAsset::class:
                /** @var BusAsset $transportable */
                return AirportBus::createFrom(
                    $transportable->origin,
                    $transportable->destination,
                    $transportable->seat
                );
                break;
            case TrainAsset::class:
                /** @var TrainAsset $transportable */
                return Train::createFrom(
                    $transportable->origin,
                    $transportable->destination,
                    $transportable->number,
                    $transportable->seat
                );
                break;
            default:
                throw new InvalidBoardType();
        }
    }
}
